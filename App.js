import {createStackNavigator, createAppContainer} from 'react-navigation';
import LoginView from './src/views/LoginView'
import HomeView from './src/views/HomeView'
import DetailNoteView from './src/views/DetailNoteView';
import CheckoutView from './src/views/CheckoutView';
import TopicsView from './src/views/TopicsView';
import QuestionaryView from './src/views/QuestionaryView';
import ForgotPassView from './src/views/ForgotPassView';
import RegisterView from './src/views/RegisterView';

const MainNavigator = createStackNavigator({
  Login: {screen: LoginView},
  Home: {screen: HomeView, navigationOptions:{ title:'Inicio'}},
  DetailNoteView: { screen: DetailNoteView, navigationOptions:{ title:'Detalle de curso'}},
  CheckoutView: { screen: CheckoutView },
  TopicsView: { screen: TopicsView, navigationOptions:{ title:'Temario'} }, 
  QuestionaryView: { screen: QuestionaryView }, 
  ForgotPassView: { screen: ForgotPassView, navigationOptions:{ title:'Olvidé mi contraseña'}  }, 
  RegisterView: { screen: RegisterView, navigationOptions:{ title:'Registrarse'}  }
});

const App = createAppContainer(MainNavigator);
export default App;
