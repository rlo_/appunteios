import eServices from './ExternalServices';
import User from '../models/User';
import Note from '../models/Note';
import Topic from '../models/Topic';
import Transaction from '../models/Transaction';
import Subtopic from '../models/Subtopic';
import Question from '../models/Question';
import Review from '../models/Review';
const Realm = require('realm');
const realm = new Realm({schema: [User, Note, Topic, Transaction, Subtopic, Question, Review], schemaVersion: 26});

export default class Api{
    deleteAll= () => {
        realm.write(() => {
            realm.deleteAll();
        });
    };

    isUserLogged= () => {
        return realm.objects('User').length > 0;
    };

    doLoginUser = async (email, password) => {
        let jsonUser = await eServices.getUserLogin(email, password);    
        if(jsonUser != null){
            try{
                realm.write(() => {
                    realm.create('User', jsonUser.data);
                });
            }catch(exception){
                return false;
            }
            return true;
        }
        return false;
    };

    getNoteData = async () => {
        let token = realm.objects('User')[0].token;
        let json = await eServices.getNotes(token);
        if(json != null){
            try{
                realm.write(() => {                    
                    if(json.data != null && json.data.length > 0){
                        realm.delete(realm.objects('Note'));
                        json.data.forEach(obj => {
                            if(obj != null){
                                realm.create('Note', obj);
                            }
                        });
                    }
                });
            }catch(exception){
                // alert(exception);
                return null;
            }
        }
        return realm.objects('Note');
    };

    getNoteDetailsData = async (itemid) => {
        note = realm.objects('Note').filtered(`id = ${itemid}`)[0];
        let token = realm.objects('User')[0].token;
        let json = await eServices.getNoteDetails(token, note.id);
        if(json != null){
            try{
                let note = json.data;
                realm.write(() => {
                    note.topics = json.data.topics;
                    note.subtopics = json.data.topics;
                    note.reviews = json.data.reviews;
                });
                return note;
            }catch(exception){
                alert(exception);
                return null;
            }
        }
        return null;
    };
    
    doRegisterUser = async (email, password, name) => {
        let jsonUser = await eServices.doRegisterUser(email, password);    
        if(jsonUser != null){
            return true;
        }
        return false;
    };

    doRequestForgotPass = async (email) => {
        return await eServices.doRequestForgotPass(email);  
    };

    getWebpayTransaction = async (noteid) => 
    {
        let token = realm.objects('User')[0].token;
        let json = await eServices.getTransaction(token, noteid)
        if(json != null){
            let transaction;
            realm.write(() => {
                transaction = realm.create('Transaction', json.data);
            });
            return transaction;
        }
        return null;
    }

    getQuestions = async (note_id, topic_id, Subtopic_id) => {
        let token = realm.objects('User')[0].token;
        let json = await eServices.getQuestions(token, note_id, topic_id, Subtopic_id)
        if(json != null){
            let questions = new Array();
            realm.write(() => {
                realm.delete(realm.objects('Question'));
                json.data.forEach(obj => {
                    questions.push(realm.create('Question', obj));
                });
            });
            return questions;
        }
        return null;
    }

    // managers
    getNoteDetails = async (itemid) => {        
        note = await this.getNoteDetailsData(itemid);
        return note;
    }
}