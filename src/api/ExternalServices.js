// appunte API
const URL = 'https://www.appunte.cl';
const URL_WITH_TOKEN = `${URL}/api`;
const axios = require('axios');

export default ExternalServices = {
  async getUserLogin(email, password) {
    let urlEndpoint = `${URL}/ionic_login?cellphone_email=${email}&password=${password}`
    return await axios.get(urlEndpoint)
    .catch(function(error){
      return null;
    });
  },
  
  async getNotes(token) {
    let urlEndpoint = `${URL_WITH_TOKEN}/${token}/notes.json`
    return await axios.get(urlEndpoint)
    .catch(function(error){
      return null;
    });
  },

  async getNoteDetails(token, noteid) {
    let urlEndpoint = `${URL_WITH_TOKEN}/${token}/notes/${noteid}.json`
    return await axios.get(urlEndpoint)
    .catch(function(error){
      return null;
    });
  },

  async getTransaction(token, noteid) {
    let urlEndpoint = `${URL_WITH_TOKEN}/${token}/transaction_create/?which_buy=full_course&note_id=${noteid}&payment_method=webpay`;
    return await axios.get(urlEndpoint)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return null;
    });
  },

  async doRegisterUser(email, password, name) {
    let urlEndpoint = `${URL}/api_post/users`
    return await axios.post(urlEndpoint, {
      email: email,
      password: password,
      name: name
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  },

  async doRequestForgotPass(email) {
    let urlEndpoint = `${URL}/reset_password_api`
    return await axios.post(urlEndpoint, {
        email: email
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return null;
    });
  },

  async getQuestions(token, noteid, topicid, subtopicid){
    let urlEndpoint = `${URL_WITH_TOKEN}/${token}/questions/${noteid}/${topicid}/${subtopicid}`;
    return await axios.get(urlEndpoint)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return null;
    });
  },
  
};
