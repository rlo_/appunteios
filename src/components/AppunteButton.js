import React from 'react';
import { StyleSheet, Text, Button, TouchableOpacity } from 'react-native';
import { Colors } from '../utils/Colors'

export default class AppunteButton extends React.Component{

    render(){
        if(this.props.buttonType === "white"){
            return (
                <TouchableOpacity
                    style={styles.button}
                    onPress={this.props.onPress}
                    underlayColor='#fff'>
                    <Text style={styles.buttonText}>{this.props.title}</Text>
                </TouchableOpacity>
            );
        }
        else if(this.props.buttonType === "blue")
        {
            return (
                <TouchableOpacity
                    style={styles.blueButton}
                    onPress={this.props.onPress}
                    underlayColor='#fff'>
                    <Text style={styles.blueButtonText}>{this.props.title}</Text>
                </TouchableOpacity>
            );
        }
    }

}

const styles = StyleSheet.create({
    button:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,   
        paddingTop:10,
        paddingBottom:10,
        backgroundColor: Colors.white,
        borderRadius:20,
        borderWidth: 1,
        borderColor: Colors.white
    },
    buttonText:{
        textAlign:'center',
        paddingLeft : 100,
        paddingRight : 100
    },
    blueButton:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,   
        paddingTop:10,
        paddingBottom:10,
        backgroundColor: '#3b5998',
        borderRadius:20,
        
    },
    blueButtonText:{
        textAlign:'center',
        color: '#fff',
        width: "250%",
        paddingLeft : "5%",
        paddingRight : "5%"
    }  
});

