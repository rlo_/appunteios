import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import {ListItem} from 'react-native-elements';

export default class CommentsView extends Component {
  state = {
    activeSections: [],
    content: this.props.content,
    sections: this.props.sections
  };

  _renderSectionTitle = section => {
    return (
      <View style={styles.content}>
        
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>{section.title}</Text>
      </View>
    );
  };

  _renderContent = () => {
    return (
      <View>
        {
        this.state.content.map((obj, i) =>  <ListItem
                                                key={i}
                                                leftAvatar={{ source: { uri: obj.user_image } }}
                                                title={obj.user_name}
                                                subtitle={obj.content}
                                                />)
      }
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    return (
      <Accordion
        sections={this.state.sections}
        activeSections={this.state.activeSections}
        renderSectionTitle={this._renderSectionTitle}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
        touchableProps={{underlayColor: "#fff"}}
      />
    );
  }
}


const styles = StyleSheet.create({
    header: {
      textAlign: 'center',
      alignItems: 'center',
      marginTop: '5%'
    },
    headerText:{
      fontSize: 18,
      textDecorationLine: 'underline'
    },
    content : {
      alignItems: 'flex-start',
      marginTop: 10
    }
  });
  