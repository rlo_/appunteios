import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import NoteRow from './NoteRow';
import HomeView from '../views/HomeView';
import Api from '../api/Api';
import { StackActions, NavigationActions } from 'react-navigation';
const api = new Api();

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class NoteListview extends React.Component {

    constructor(props){
      super(props);      
    }
    
    componentDidMount(){
    }

    render() {
        return (
            <View style={styles.container}>
            <FlatList
                data={this.props.itemList}
                renderItem={({ item }) => 
                <NoteRow
                    id={item.id}
                    name={item.name}
                    teacher={item.teacher}
                    preview={item.preview}
                    onpress={this.props.onpressitem}
                />}
                keyExtractor={item => item.name}
            />
        </View>
        );
      }
}