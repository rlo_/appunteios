import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, ActivityIndicator} from 'react-native';
import { Image } from 'react-native-elements';

export default class NoteRow extends React.Component {

    constructor(props){
      super(props);      
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.props.onpress.bind(this, this.props.id)}>
            <View style={styles.container}>
                <Image source={{ uri: this.props.preview }} style={styles.photo} placeholderStyle={styles.placeholderimg} PlaceholderContent={<ActivityIndicator />}/>
                <View style={styles.container_text}>
                    <Text style={styles.title}>
                        {this.props.name}
                    </Text>
                    <Text style={styles.description}>
                        {this.props.teacher}
                    </Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
        );
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:2,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 70,
        width: 70,
    },
    placeholderimg:{
        backgroundColor: '#fff'
    }
});