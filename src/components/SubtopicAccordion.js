import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import {Card} from 'react-native-elements'

export default class SubtopicAccordion extends Component {
  state = {
    activeSections: [],
    content: this.props.content,
    sections: this.props.sections,
    noteid: this.props.noteid
  };

  _renderSectionTitle = section => {
    return (
      <View style={styles.content}>
        
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <Card>
        <Text style={styles.headerText}>{section.title}</Text>
      </Card>
    );
  };

  _renderContent = () => {
    return (
      <Card style={styles.content}>
        {this.state.content.map((obj, i) => <View style={styles.contentItem}  key={i}><Text style={styles.item} onPress={this._openQuestionsActivity.bind(this, obj)}>{obj.name}</Text><Text style={styles.row} onPress={this._openQuestionsActivity.bind(this, obj)}> > </Text></View>)}
      </Card>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  _openQuestionsActivity = (sb) => {
    let noteid = this.state.noteid;
    this.props.navigation.navigate('QuestionaryView', {note_id: noteid, topic_id: sb.topic_id, subtopic_id: sb.id});
  };

  render() {
    return (
      <Accordion
        sections={this.state.sections}
        activeSections={this.state.activeSections}
        renderSectionTitle={this._renderSectionTitle}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
        touchableProps={{underlayColor: "#fff"}}
      />
    );
  }
}


const styles = StyleSheet.create({
    header: {
      textAlign: 'center',
      alignItems: 'center',
      marginTop: '5%'
    },
    headerText:{
      fontSize: 14,
      color: '#0794d8',
      textDecorationLine: 'underline'
    },
    content : {
      alignItems: 'center',
      marginTop: 5,
    },
    contentItem: {
      flexDirection: 'row',
      paddingRight: 5,
      paddingLeft: 5
    },
    item: {
      flex:1,
      height: 40,
      width: "100%",
      fontSize: 12,
      textAlignVertical: 'center',
      textAlign: 'left',
      borderBottomColor:'#000',
      borderBottomWidth: 2
    },
    row: {
      textAlign: 'right',
      flex: 1,
      fontSize: 12,
      color: '#0794d8'
    }
  });
  