import React from 'react';
import { View, Text, StyleSheet, Image, TouchableWithoutFeedback } from 'react-native';
import SubtopicAccordion from './SubtopicAccordion';




export default class TopicRow extends React.Component {

    constructor(props){
      super(props);      
    }

    render() {
        return (
            <SubtopicAccordion style={styles.accordion}
                               noteid={this.props.noteid}
                               sections={[{title: this.props.name}]} 
                               navigation={this.props.navigation}
                               content={this.props.subtopics}>
            </SubtopicAccordion>
        );
      }
}

const styles = StyleSheet.create({
    container: {
     flex: 1
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 70,
        width: 70,
    },
});