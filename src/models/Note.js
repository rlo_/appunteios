export default class Note {}
Note.schema = {
    name: 'Note',
    properties: {
        id: 'int',
        name: 'string?',
        teacher: 'string?',
        price: 'int?',
        preview: 'string?',
        description: 'string?',
        buyed: 'bool?',
        questions: 'int?',
        users: 'int?',
        topics: 'Topic[]',
        reviews: 'Review[]',
        subtopics: 'Subtopic[]'
    },
};