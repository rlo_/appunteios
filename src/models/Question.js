export default class Question {}
Question.schema = {
    name: 'Question',
    properties: {
        id: 'int?',
        subtopic_id: 'int?',
        sort: 'int?',
        answer_content: 'string?',
        question_content: 'string?',
        subtopic: 'string?',
        topic: 'string?',
        note: 'string?'
    },
};