export default class Review {}
Review.schema = {
    name: 'Review',
    properties: {
        content: 'string?',
        user_image: 'string?',
        user_name: 'string?',
        rating: 'int?',
        time_ago: 'string?'
    },
};