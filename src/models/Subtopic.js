export default class Subtopic {}
Subtopic.schema = {
    name: 'Subtopic',
    properties: {
        id: {type: 'int', optional: true},
        name: {type: 'string', optional: true},
        sort: {type: 'int', optional: true},
        topicId: {type: 'int', optional: true}
    },
};