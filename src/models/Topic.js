export default class Note {}
Note.schema = {
    name: 'Topic',
    properties: {
        id: {type: 'int', optional: true},
        name: {type: 'string', optional: true},
        questions: {type: 'int', optional: true}
    },
};