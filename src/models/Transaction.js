export default class Transaction {}
Transaction.schema = {
    name: 'Transaction',
    properties: {
        url: 'string?',
        token: 'string?'
    },
};