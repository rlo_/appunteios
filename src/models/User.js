export default class User {}
User.schema = {
    name: 'User',
    properties: {
        first_name: {type: 'string', optional: true},
        last_name: {type: 'string', optional: true},
        email: {type: 'string', optional: true},
        username: {type: 'string', optional: true},
        token: {type: 'string', optional: true},
        avatar_image: {type: 'string', optional: true},
        facebook_id: {type: 'string', optional: true},
        name: {type: 'string', optional: true},
        age: {type: 'int', default: 0},
    },
};