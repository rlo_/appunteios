import { Colors } from '../utils/Colors';

export default {
    container: {
        flex: 1,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
      },
      wellDiv: {
        width: 280,
        paddingTop:10,  
        paddingBottom:10,
        backgroundColor: Colors.white,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textInput: {
        height: 30,
        marginTop: 10,
        marginBottom: 10,
        borderBottomColor: Colors.primaryDark,
        borderBottomWidth: 1,
        width: 250
      },
      title: {
          color: Colors.white,
          fontSize: 25,
          marginBottom: 20
      },
      logo: {
        width:200,
        height:150,
        marginBottom: 20,
        resizeMode: 'contain'
      },
      textButtons: {
        color: '#fff',
        marginTop: "3%",
        fontSize: 16
      }
}