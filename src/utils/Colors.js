export const Colors = {
    white: '#fff',
    primary: '#337AB7',
    primaryDark: '#337AB0',
    blueLink: '#37beff'
}