export default class Utils {
    numberWithCommas = (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
};