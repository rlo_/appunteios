import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, TouchableOpacity } from 'react-native';
import Api from '../api/Api';
import Utils from '../utils/Utils';
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from 'react-native-gesture-handler';
import { CustomTabs } from 'react-native-custom-tabs';
import { Linking } from 'react-native';

const URL = 'https://www.appunte.cl/';
const api = new Api();
const utils = new Utils();


export default class CheckoutView extends React.Component {
    constructor(props){
      super(props);
      this.state = { 
          note: this.props.navigation.state.params.note,
          spinner: true
      }
    }

    componentDidMount() {
        this.setState({
            spinner: false
        });
    }

    render() {
      if(this.state.spinner){
        return (
          <View style={styles.container}>
            <Spinner
              visible={this.state.spinner}
              textStyle={styles.spinnerTextStyle}
            />
          </View>
        );
      }
      else{
        return (
            <View style={styles.body}>
                <ScrollView>
                    <View style={styles.container}>
                        <Image source={{ uri: this.state.note.preview }} style={styles.photo}/>
                        <View style={styles.container_text}>
                            <Text style={styles.title}>
                                {this.state.note.name}
                            </Text>
                            <Text style={styles.price}>
                                $ {utils.numberWithCommas(this.state.note.price)} CLP
                            </Text>
                        </View>
                    </View>
                    <Text style={styles.total_price}>
                        Total a pagar: $ {utils.numberWithCommas(this.state.note.price)} CLP
                    </Text>
                </ScrollView>
                <TouchableOpacity
                style={styles.btn_buy}
                onPress={this._goToPay}
                underlayColor='#fff'>
                    <Text style={styles.btn_buy_text}>Ir a pagar</Text>
                </TouchableOpacity>
            </View>
        );
      }
    }

    _goToPay = async () => {
        let transaction = await api.getWebpayTransaction(this.state.note.id);
        let urlWebpay = `${URL}webpay_mobile/?token=${transaction.token}&url=${transaction.url}`;
        this.openWebpay(urlWebpay);
    }

    openWebpay = (urlTransaction) => { 
        Linking.openURL(urlTransaction);
        this.props.navigation.pop(2);
    };
}


const styles = StyleSheet.create({
    body: {
        flexDirection: 'column',
        height: '99%'
    },
    container: {
        flexDirection: 'row',
        padding: 10,
        marginLeft:2,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    price: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    photo: {
        height: 70,
        width: 70,
    },
    total_price: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    btn_buy: {
        backgroundColor: "#f779a4",
        position: 'absolute',
        bottom: 10,
        padding: 12,
        width: "100%",
        color: '#fff'
      },
    btn_buy_text: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16
    }
});