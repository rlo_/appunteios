import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity } from 'react-native';
import Api from '../api/Api';
import Utils from '../utils/Utils';
import CommentsView from '../components/CommentsView';
import AccordionView from '../components/AccordionView';
import Spinner from 'react-native-loading-spinner-overlay';

const api = new Api();
const utils = new Utils();

const SECTIONS_TOPICS = [
  {
    title: 'Temario'
  }
];

const SECTIONS_COMMENTS = [
  {
    title: 'Comentarios'
  }
];

export default class DetailNoteView extends React.Component {
    constructor(props){
      super(props);
      this.state = {
          note: [],
          navigation: this.props.navigation,
          topics: [],
          reviews: [],
          spinner: true
      };
      this._getNote(this.state.navigation.state.params.itemid);
    }
    

    render() {
      if(this.state.spinner){
        return (
          <View style={styles.container}>
            <Spinner
              visible={this.state.spinner}
              textStyle={styles.spinnerTextStyle}
            />
          </View>
        );
      }
      else{
        const note = this.state.note;
        const topics = Array.from(note.topics);
        const reviews = Array.from(note.reviews);
        console.log(reviews)
        return (
          <View style={styles.container}>
            <ScrollView>
                <Image source={{ uri: note.preview }} style={styles.photo}/>
                <View style={styles.container_text}>
                      <Text style={styles.title}>
                          {note.name}
                      </Text>
                      <Text style={styles.description}>
                          {note.teacher}
                      </Text>
                      <Text style={styles.price}>
                          $ {utils.numberWithCommas(note.price)} CLP
                      </Text>
                </View>
                <View style={styles.container_counts}>
                      <View style={styles.questions}>
                          <Image source={require('../../assets/questions.png')} style={styles.icon}/>
                          <Text>{note.questions}</Text>
                          <Text>Preguntas</Text>
                      </View>
                      <View style={styles.users}>
                          <Image source={require('../../assets/users.png')} style={styles.icon}/>
                          <Text>{note.users}</Text>
                          <Text>Usuarios</Text>
                      </View>
                </View>
              <AccordionView style={styles.accordion} sections={SECTIONS_TOPICS} content={topics}/>
              <CommentsView style={styles.accordion} sections={SECTIONS_COMMENTS} content={reviews}/>
            </ScrollView>
            <TouchableOpacity
              style={styles.btn_buy}
              onPress={note.buyed ? this.navigateToTopics.bind() : this._buyNote.bind(this, note)}
              underlayColor='#fff'>
                <Text style={styles.btn_buy_text}>{note.buyed ? "Ver appunte" : "Comprar appunte"}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }

    _buyNote = (note) => {this.props.navigation.navigate('CheckoutView', {note: note})};

    _getNote = async (itemid) => {
      let result = await api.getNoteDetails(itemid);
      if(result.buyed){
        this.navigateToTopics();
      }
        this.setState({
          note: result,
          topics: result.topics,
          reviews: result.reviews,
          spinner: false
        });

      console.log(result.reviews);
    }

    navigateToTopics = () => this.props.navigation.navigate('TopicsView', {note: note});
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      flexDirection: 'column',
      marginBottom: 8,
      borderRadius: 5,
      backgroundColor: '#FFF',
      elevation: 2,
  },
  accordion:{
    textAlign: 'center',
    justifyContent: 'center',
  },
  title: {
      fontSize: 16,
      color: '#000',
  },
  container_text: {
      flexDirection: 'column',
      marginTop: 10,
      alignItems: 'center',
      justifyContent: 'center',
  },
  container_counts: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  description: {
      fontSize: 11,
      fontStyle: 'italic',
  },
  price: {
    fontSize: 12,
    backgroundColor: '#21db24',
    marginTop: 15,
    borderRadius: 20,
    padding: 5
  },
  photo: {
      height: 150,
      width: '100%',
  },
  questions: {
    textAlign: 'center',
    alignItems: 'center'
  },
  users: {
    textAlign: 'center',
    alignItems: 'center'
  },
  icon: {
    height:30,
    justifyContent:'center',
    resizeMode: 'contain'
  },
  btn_buy: {
    backgroundColor: "#f779a4",
    position: 'absolute',
    bottom: 10,
    padding: 12,
    width: "100%",
    color: '#fff'
  },
  btn_buy_text: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16
  }
});
