import React from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import Api from '../api/Api';
import NoteListView from '../components/NoteListView';
import { StackActions, NavigationActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
import { HeaderBackButton } from 'react-navigation';
const api = new Api();



export default class HomeView extends React.Component {
 

static navigationOptions = ({navigation}) => {
  return{
    headerLeft:(<HeaderBackButton onPress={_logout.bind(this, navigation)}/>)
 }
}
    constructor(props){
      super(props);
      this.state = {
          notes: [],
          spinner: true
      }
      this._getNotes();
    }

    render() {
      if(this.state.spinner){
          return (
            <View style={styles.container}>
              <Spinner
                visible={this.state.spinner}
                textStyle={styles.spinnerTextStyle}
              />
            </View>
          );
        }
        else{
          return (
            <View style={styles.container}>
                    <NoteListView
                        itemList={this.state.notes}
                        onpressitem={this._goToDetails}
                    />
            </View>
          );
        }
      }
      
      _getNotes = async () => {
          var result = await api.getNoteData();
          result = Array.from(result);
          this.setState({
            notes: result,
            spinner: false
          });
      };

      _goToDetails = async (itemid) => {
        this.props.navigation.navigate('DetailNoteView', {itemid});
      }
 
    }

    _goToLogin = async (navigation) => {
      const resetAction = StackActions.reset({
        index: 0, // <-- currect active route from actions array
        actions: [
          NavigationActions.navigate({ routeName: 'Login' }),
        ],
      });
      navigation.dispatch(resetAction);
    }

    _logout = (navigation) => {
      // Works on both iOS and Android
      Alert.alert(
          '¿Estas seguro?',
          'Deseas cerrar sesión?',
          [
            {text: 'Aceptar', onPress: () => {api.deleteAll(); _goToLogin(navigation);}},
            {text: 'Cancelar', onPress: () => console.log('Cancelado')},
          ],
          {cancelable: false},
        );
      };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FCFCFC',
  }
});
