import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, Alert } from 'react-native';
import AppunteButton from '../components/AppunteButton';
import styles from '../styles/LoginView.style';
import Api from '../api/Api';
import Spinner from 'react-native-loading-spinner-overlay';
import { StackActions, NavigationActions } from 'react-navigation';

const api = new Api();


export default class LoginView extends React.Component {
  state = {
    email: '',
    password: '',
    spinner: false
  }
  static navigationOptions = {
    title: 'Login',
  };

  componentDidMount(){
     this._checkSession(false);
  }

  render() {
    const {navigate} = this.props.navigation;
    const view = (<View style={styles.container}>
      <Image 
          style={styles.logo}
          source={require('../../assets/logo_short.png')}/>
      <Text style={styles.title}>Appunte</Text>
      <View style={styles.wellDiv}>
          <TextInput 
              onChangeText={email => this.setState({email})}
              style={styles.textInput}
              placeholder="Correo electronico"/>
          <TextInput 
              secureTextEntry={true}
              onChangeText={password => this.setState({password})}
              style={styles.textInput}
              placeholder="Contraseña"/>
      </View>
      <AppunteButton title="Iniciar sesión" buttonType="white" onPress={this._doLoginUser}/>
      {/* <AppunteButton title="Iniciar sesión con Facebook" buttonType="blue" onPress={this._doLoginUser}/> */}
      <Text style={styles.textButtons} onPress={this._goToRegister.bind(this)}>Registrarse</Text>
      <Text style={styles.textButtons} onPress={this._goToForgot.bind(this)}>Olvidé mi contraseña</Text>
    </View>);

    if(this.state.spinner){
      return (
        <View style={styles.container}>
          <Spinner
            visible={this.state.spinner}
            textStyle={styles.spinnerTextStyle}
          />
           {view}
        </View>
        
      );
    }
    else{
      return (view);
    }
  }

  _checkSession = async (showMessageError) => {
    if((await api.isUserLogged())){
      this._goToHome();
    }else if(showMessageError){
      Alert.alert(
        'Datos incorrectos',
        'Los datos ingresados no son validos para el inicio de sesión',
        [
          {text: 'Aceptar', onPress: () => this.setState({spinner: false}) }
        ],
        {cancelable: false},
      );
    };
  }

  _goToRegister = () => {
    this.props.navigation.navigate('RegisterView')
  };

  _goToForgot = () => {
    this.props.navigation.navigate('ForgotPassView')
  };

  _goToHome = async () => {
    const resetAction = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      actions: [
        NavigationActions.navigate({ routeName: 'Home' }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  _doLoginUser = async () => { 
    const { email, password } = this.state
    this.setState({spinner: true})
    await api.doLoginUser(email, password); 
    await this._checkSession(true);
  }
}