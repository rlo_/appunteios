import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image } from 'react-native';
import { Card } from 'react-native-elements'
import Api from '../api/Api';
import NoteListView from '../components/NoteListView';
import { StackActions, NavigationActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
const api = new Api();


export default class HomeView extends React.Component {
    constructor(props){
      super(props);
      this.state = {
          note_id: this.props.navigation.state.params.note_id,
          topic_id: this.props.navigation.state.params.topic_id,
          subtopic_id: this.props.navigation.state.params.subtopic_id,
          questions: null,
          currentQuestionIndex: 0,
          maxQuestionIndex: 0,
          isAnswer: false,
          spinner: true
      }
      this._getQuestions(this.state.note_id, this.state.topic_id,  this.state.subtopic_id);
    }

    render() {
      if(this.state.spinner){
        return (
          <View style={styles.container}>
            <Spinner
              visible={this.state.spinner}
              textStyle={styles.spinnerTextStyle}
            />
          </View>
        );
      }
      else{
        return (
          <View style={styles.container}>
            <Card titleStyle={styles.titleStyle} title={`${this.state.isAnswer ? "Respuesta" : "Pregunta"} (${this.state.currentQuestionIndex + 1}/${this.state.maxQuestionIndex})`}>
              <Text>{this._getCurrentQuestion()} </Text>
            </Card>
            <View  style={styles.container_btns}>
              <Button title="< Ant." onPress={this._backQuestion.bind(this)} ></Button>
              <Button title={ "Ver " + (this.state.isAnswer ? "Pregunta" : "Respuesta")} onPress={this._toggleViewAnswerOrQuestion.bind(this)} ></Button>
              <Button title="Sig. >" onPress={this._nextQuestion.bind(this )} ></Button>
            </View>
          </View>
        );
      }
    }
    
    
  
    
    _toggleViewAnswerOrQuestion = () => {
      this.setState({isAnswer: !this.state.isAnswer})
    }

    _backQuestion = () => {
      if(this.state.currentQuestionIndex > 0)
        this.setState({
          currentQuestionIndex: --this.state.currentQuestionIndex,
          isAnswer: false
        })
    }

    _nextQuestion = () => {
      if(this.state.currentQuestionIndex < this.state.maxQuestionIndex)
        this.setState({
          currentQuestionIndex: ++this.state.currentQuestionIndex,
          isAnswer: false
        })
    }

    _getCurrentQuestion = () => {
      let questions = this.state.questions;
      let content = null;
      if(questions != null) {
        if(this.state.isAnswer){
          content = questions[this.state.currentQuestionIndex].answer_content;
        }else{
          content = questions[this.state.currentQuestionIndex].question_content;
        }
      }
      return content
    }

    _getQuestions = async (note_id, topic_id, subtopic_id) => {
        let questions = await api.getQuestions(note_id, topic_id, subtopic_id);
        
        this.setState({
          questions: questions,
          maxQuestionIndex: questions.length-1,
          spinner: false
        });
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FCFCFC',
  },
  container_btns: {
    flex: 1,
    position: 'absolute',
    bottom: 15,
    width: '100%',
    alignContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleStyle: {
    color: '#0794d8'
  }
});
