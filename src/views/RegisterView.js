import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, Alert } from 'react-native';
import AppunteButton from '../components/AppunteButton';
import styles from '../styles/LoginView.style';
import Api from '../api/Api';
import { StackActions, NavigationActions } from 'react-navigation';

const api = new Api();


export default class RegisterView extends React.Component {
  state = {
    email: '',
    password: '',
    name: '',
    repassword: ''
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <Image 
            style={styles.logo}
            source={require('../../assets/logo_short.png')}/>
        <Text style={styles.title}>Appunte</Text>
        <View style={styles.wellDiv}>
            <TextInput 
                onChangeText={name => this.setState({name})}
                style={styles.textInput}
                placeholder="Nombre completo"/>
            <TextInput 
                onChangeText={email => this.setState({email})}
                style={styles.textInput}
                placeholder="Correo electronico"/>  
            <TextInput 
                secureTextEntry={true}
                onChangeText={password => this.setState({password})}
                style={styles.textInput}
                placeholder="Contraseña"/>
            <TextInput 
                secureTextEntry={true}
                onChangeText={repassword => this.setState({repassword})}
                style={styles.textInput}
                placeholder="Repetir Contraseña"/>  
        </View>
        <AppunteButton title="Registrarse" buttonType="white" onPress={this._doForgotPass}/>
      </View>
    );
  }

  _goToLogin = async () => {
    const resetAction = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      actions: [
        NavigationActions.navigate({ routeName: 'Login' }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  _doForgotPass = async () => { 
    const { email, password, name, repassword } = this.state
    if (password != repassword){
      alert("Contraseñas no coinciden, por favor validar que ambas contraseñas sean iguales");
      return;
    }

    if( await api.doRegisterUser(email, password, name) ) {
      // Works on both iOS and Android
      Alert.alert(
        'OK',
        'Te has registrado con éxito, ahora puedes iniciar sesión',
        [
          {text: 'OK', onPress: () => this._goToLogin()},
        ],
        {cancelable: false},
      );

      this._goToLogin();
    }else{
      alert("¡Uppsss no salió como queríamos! Ha ocurrido un error.")
    }
    
  }
}