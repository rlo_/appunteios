import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, TouchableOpacity, FlatList } from 'react-native';
import TopicRow from '../components/TopicRow';
import Spinner from 'react-native-loading-spinner-overlay';


export default class TopicsView extends React.Component {
    constructor(props){
      super(props);
      this.state = { 
          note: this.props.navigation.state.params.note,
          spinner: true
      }
    }

    componentDidMount() {
        this.setState({
            spinner: false
        });
    }

    render() {
      if(this.state.spinner){
        return (
          <View style={styles.container}>
            <Spinner
              visible={this.state.spinner}
              textStyle={styles.spinnerTextStyle}
            />
          </View>
        );
      }
      else{
        let noteid = this.state.note.id;
        return (
            <View style={styles.body}>
                <View style={styles.container}>
                  <FlatList
                      data={this.state.note.topics}
                      renderItem={({ item }) => 
                      <TopicRow
                          name={item.name}
                          subtopics={item.subtopics}
                          noteid={noteid}
                          navigation={this.props.navigation}
                      />}
                      keyExtractor={item => item.name}
                  />
                </View>
            </View>
        );
      }
    }
}


const styles = StyleSheet.create({
  container: {
    height: "100%"
  }
});